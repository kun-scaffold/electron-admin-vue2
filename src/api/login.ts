import request from '@/utils/request';
import qs from 'qs'

// 加密登录
export async function ApiGetPublicKey(): Promise<any>  {
  // alert("data"+JSON.stringify(data))
  return request({
    // url: 'http://192.168.0.205:8900/user/api/getPublicKey',
    url: 'userlogin/user/api/getPublicKey',
    method: 'get',
    data: {}
  })
}

export function ApiloginByEncrypt(param) {
  return request({
    url: '/userlogin/user/api/loginByEncrypt',
    method: 'post',  
    data: param
  })
}

export  function queryListRfidLabel(data) {
  return request({
    url: '/rfidadapter/archive/pageInfo',
    method: 'post',
    data:data,
  });
}
