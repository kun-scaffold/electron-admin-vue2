import request from '@/utils/request-kun.js';
// import request from '@/utils/request';
import qs from 'qs'

export async function getData(params): Promise<any> {
  return request({
    url:'http://192.168.0.205:8946/ljd-rfid-adapter-msvs/v1.1/archive/pageInfo',
    method: 'POST',
    data: params,
  });
}

//分页查询，提供给RFID
export function apiGetRfidArchiveList(data) {
  return request({
    // url: '/rfidAdapter/archive/pageInfo',
    url:'http://192.168.0.205:8946/ljd-rfid-adapter-msvs/v1.1/archive/pageInfo',
    // url:'http://192.168.254.16:8946/ljd-rfid-adapter-msvs/v1.1/archive/pageInfo',
    method: 'POST',     
    data:  data,

  })
}
//根据RFID查询档案数据
export function apiGetArchiveByKey(key) {
  return request({
      url: `/rfidAdapter/archive/getByKey/${key}`,
      method: 'get',
      data: {}
  })
}

//档案注册回调接口 ‘CANCEL’(代表注销)
export function apiRegCallback(archiveId,rfid) {
  return request({
      url: `/rfidAdapter/archive/regCallback/${archiveId}/${rfid}`,
      method: 'get',
      data: {}
  })
}

//档案上架回调接口
export function apiUpShelfCallback(archiveId,layerId) {
  return request({
      url: `/rfidAdapter/archive/upAr/${archiveId}/${layerId}`,
      method: 'get',
      data: {}
  })
}
//下架回调接口
export function apiDownShelfCallback(archiveId) {
  return request({
      url: `/rfidAdapter/archive/downAr/${archiveId}`,
      method: 'get',
      data: {}
  })
}
//出库、借阅报警接口
export function apiOutRoomAlarm(rfid) {
  return request({
      url: `/rfidAdapter/archive/outAlarm/${rfid}`,
      method: 'get',
      data: {}
  })
}

//借阅归还-回调接口 type IN:入库+归还，OUT:出库+借阅
export function apiReturnCallback(archiveId,type,userId) {
  return request({
      url: `/rfidAdapter/archive/inAndOut/${archiveId}/${type}/${userId}`,
      method: 'get',
      data: {}
  })
}

//批量上下架
export function apiUpDownBatch(data) {

  return request({
    url: '/rfidAdapter/archive/upDownBatch',
      method: 'post',
    data:  data,

  })
}
