import request from '@/utils/request';
import qs from 'qs'

// 加载登录信息
export function ApiGetConfig():any {
  let url = ''
  if (process.env.NODE_ENV === 'development') {
    url = 'config-dev.json'
  } else if (process.env.NODE_ENV === 'production') {
    url = 'config-prd.json'
  }
  return request({
    url: '/config/' + url,
    method: 'get',  
    data: {}
  })  
}

export async function ApiGetConfig2(): Promise<any> {
  let url = ''
  if (process.env.NODE_ENV === 'development') {
    url = 'config-dev.json'
  } else if (process.env.NODE_ENV === 'production') {
    url = 'config-prd.json'
  }
  return request({
    url: '/config/' + url,
    method: 'get',  
    data: {}
  });
}

// request({
//   headers: { 'Content-Type': 'multipart/form-data' },
//   url: '/uploads',
//   method: 'POST',
//   data: param,
// })
// .then(res => {
//   const { data } = res;
//   resolve({
//       default: data.url || '',
//   });
// })
// .catch(err => {
//   console.log(err);
//   reject(err);
// });
