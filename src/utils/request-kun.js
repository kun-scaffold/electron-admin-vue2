import axios from 'axios'
import store from '@/config/store'
const clearRequest = {
  source: {
    token: null,
    cancel: null
  }
}
const cancelToken = axios.CancelToken
const source = cancelToken.source()

// 创建 axios 实例
const service = axios.create({
  cancelToken: source.token,
  timeout: 6000 // 请求超时时间
})

// request 拦截器
// service.interceptors.request.use(
//   config => {
//     config.cancelToken = clearRequest.source.token
//     if(config.url.indexOf('ljd') !== -1){
//       config.url += '?token=SnVwZ0VsMnNqSWtpcmk5MXFzOEVwQT09'
//     }

//     //token本应加在header里
//     return config
//   },
//   error => {
//     return Promise.reject(error)
//   }
// )
// 全局请求头中添加token   请求拦截
service.interceptors.request.use(request => {
  if (request.url.indexOf('login') < 0) {
  // let token = localStorage.getItem("token");
    const token = store.state.user.token
    // console.log("requesttoken...............   "+'Bearer ' + token)
    if (token) {
      request.headers['Authorization'] = 'Bearer ' + token
      // 数据加密
      // request.data = encrypt(JSON.stringify(config.request))
    }
  }

  // get 方法序列化参数
  // if (request.method === 'get') {
  //   request.paramsSerializer = function(params) {
  //     return qs.stringify(params, { arrayFormat: 'repeat' })
  //   }
  // }

  return request
}, error => {
  return Promise.reject(error)
})

// response 拦截器
service.interceptors.response.use(
  resp => {
    // console.log('interceptors.resp:', JSON.stringify(resp))

    // if (resp.data.status !== 200) {
    //   return Promise.reject(resp)
    // }
    if (resp.status === 401 || resp.data.status === 401) {
      location.href = '/#/login'
    }

    return resp
  },
  error => {
    console.log('interceptors.error:', error)
    if (error.response.status === 401) {
      // 401 说明 token 验证失败
      // 可以直接跳转到登录页面，重新登录获取 token
      console.log('interceptors.error:', error)
      location.href = '/#/login'
    } else if (error.response.status === 500) {
      // 服务器错误
      // do something
      return Promise.reject(error.response.data)
    }
    // 返回 response 里的错误信息
    return Promise.reject(error)
  }
)

export { clearRequest }
export default service
