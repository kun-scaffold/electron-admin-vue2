/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = require('./both_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.BothServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.BothServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.BothRequest,
 *   !proto.BothResponse>}
 */
const methodDescriptor_BothService_start1 = new grpc.web.MethodDescriptor(
  '/BothService/start1',
  grpc.web.MethodType.UNARY,
  proto.BothRequest,
  proto.BothResponse,
  /**
   * @param {!proto.BothRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.BothResponse.deserializeBinary
);


/**
 * @param {!proto.BothRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.BothResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.BothResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.BothServiceClient.prototype.start1 =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/BothService/start1',
      request,
      metadata || {},
      methodDescriptor_BothService_start1,
      callback);
};


/**
 * @param {!proto.BothRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.BothResponse>}
 *     Promise that resolves to the response
 */
proto.BothServicePromiseClient.prototype.start1 =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/BothService/start1',
      request,
      metadata || {},
      methodDescriptor_BothService_start1);
};


module.exports = proto;

