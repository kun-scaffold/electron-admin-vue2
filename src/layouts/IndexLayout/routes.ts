import settings from "@/config/settings";
import { RoutesDataItem } from "@/utils/routes";
import BlankLayout from '@/layouts/BlankLayout.vue';


const IndexLayoutRoutes1: Array<RoutesDataItem> = [

  // 首页
  {
    icon: 'home',
    title: 'index-layout.menu.home',
    path: '/home',
    redirect: settings.homeRouteItem.path,
    component: BlankLayout,
    children: [
      {
        ...settings.homeRouteItem
      },
      {
        icon: 'edit',
        title: 'index-layout.menu.home.custom-breadcrumbs',
        path: 'custombreadcrumbs',
        component: ()=> import('@/views/custom-breadcrumbs/index.vue'),
        breadcrumb: [
          {
            title: 'index-layout.menu.home.custom-breadcrumbs',
            path: '/home/custombreadcrumbs',
          },
          {
            title: 'index-layout.menu.home',
            path: '/home',
          },
          {
            title: 'index-layout.menu.home.custom-breadcrumbs.liqingsong.cc',
            path: 'http://liqingsong.cc',
          },
        ],
        tabNavCloseBefore: (close: () => void): void=> {
          if(window.confirm('确认关闭吗')) {
            close();
          }
        }
      },
      {
        icon: 'detail',
        title: 'index-layout.menu.home.docs',
        path: 'http://admin-element-vue.liqingsong.cc/',
        belongTopMenu: '/home',
        redirect: ''
      },
    ],
  },
// 组件示例
  {
    icon: 'components',
    title: 'index-layout.menu.component',
    path: '/component',
    redirect: '/component/icon/svg',
    component: BlankLayout,
    children:[
        {
            icon: 'icon',
            title: 'index-layout.menu.component.icon',
            path: 'icon',
            redirect: '/component/icon/svg',
            component: BlankLayout,
            children: [
                {
                    title: 'index-layout.menu.component.icon.svg',
                    path: 'svg',
                    component: () => import('@/views/component/icon/svg/index.vue'),
                },
                {
                  title: 'index-layout.menu.component.icon.font',
                  path: 'font',
                  component: () => import('@/views/component/icon/font/index.vue'),
                },
            ]
        },
        {
            icon: 'editor',
            title: 'index-layout.menu.component.editor',
            path: 'editor',
            redirect: '/component/editor/tuieditor',
            component: BlankLayout,
            children: [
                {
                    title: 'index-layout.menu.component.editor.tui-editor',
                    path: 'tuieditor',
                    component: () => import('@/views/component/editor/tui-editor/index.vue'),
                },
                {
                    title: 'index-layout.menu.component.editor.ckeditor',
                    path: 'ckeditor',
                    component: () => import('@/views/component/editor/ckeditor/index.vue'),
                }
            ]
        }
    ]
  },
// 页面示例
  {
    icon: 'page',
    title: 'index-layout.menu.pages',
    path: '/pagesample',
    redirect: '/pagesample/list/table',
    component: BlankLayout,
    children: [
      {
        icon: 'list',
        title: 'index-layout.menu.pages.list',
        path: 'list',
        redirect: '/pagesample/list/table',
        component: BlankLayout,
        children: [          
          {
            title: 'index-layout.menu.pages.list.table',
            path: 'table',
            component: ()=> import('@/views/pagesample/list/table/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.list.highly-adaptive-table',
            path: 'highlyadaptivetable',
            component: ()=> import('@/views/pagesample/list/highly-adaptive-table/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.list.search',
            path: 'search',
            redirect: '/pagesample/list/search/table',
            component: BlankLayout,
            children: [
              {
                title: 'index-layout.menu.pages.list.search.table',
                path: 'table',
                component: ()=> import('@/views/pagesample/list/search/table/index.vue'),
              }              
            ],
          },
        ],
      },
      {
        icon: 'edit',
        title: 'index-layout.menu.pages.form',
        path: 'form',
        redirect: '/pagesample/form/basic',
        component: BlankLayout,
        children: [
          {
            title: 'index-layout.menu.pages.form.basic',
            path: 'basic',
            component: ()=> import('@/views/pagesample/form/basic/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.form.complex',
            path: 'complex',
            component: ()=> import('@/views/pagesample/form/complex/index.vue'),
          },
        ],
      },
      {
        icon: 'detail',
        title: 'index-layout.menu.pages.detail',
        path: 'detail',
        redirect: '/pagesample/detail/basic',
        component: BlankLayout,
        children: [
          {
            title: 'index-layout.menu.pages.detail.basic',
            path: 'basic',
            component: ()=> import('@/views/pagesample/detail/basic/index.vue'),
            tabNavType: 'querypath',
          },
          {
            title: 'index-layout.menu.pages.detail.module',
            path: 'module',
            component: ()=> import('@/views/pagesample/detail/module/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.detail.table',
            path: 'table',
            component: ()=> import('@/views/pagesample/detail/table/index.vue'),
          },
        ],
      },
    ],
  },
// 权限验证
  {
    icon: 'permissions',
    title: 'index-layout.menu.roles',
    path: '/roles',
    redirect: '/roles/all',
    component: BlankLayout,
    children: [
      {
        icon: 'detail',
        title: 'index-layout.menu.roles.all',
        path: 'all',
        component: ()=> import('@/views/roles/all/index.vue'),
      },
      {
        icon: 'detail',
        roles: ['user'],
        title: 'index-layout.menu.roles.user',
        path: 'user',
        component: ()=> import('@/views/roles/user/index.vue'),
      },
      {
        icon: 'detail',
        roles: ['test'],
        title: 'index-layout.menu.roles.test',
        path: 'test',
        component: ()=> import('@/views/roles/test/index.vue'),
      },
    ],
  },
  // ============================================
 


];

 // ============================================
//  自定义菜单
const IndexLayoutRoutes: Array<RoutesDataItem> = [

  // 首页
  {
    hidden: false,
    icon: 'home',
    title: 'index-layout.menu.home',
    path: '/home',
    redirect: settings.homeRouteItem.path,
    component: BlankLayout,
    children: [
      {
        ...settings.homeRouteItem
      },
      {
        hidden: false,
        icon: 'edit',
        title: 'index-layout.menu.home.custom-breadcrumbs',
        path: 'custombreadcrumbs',
        component: ()=> import('@/views/custom-breadcrumbs/index.vue'),
        breadcrumb: [
          {
            title: 'index-layout.menu.home.custom-breadcrumbs',
            path: '/home/custombreadcrumbs',
          },
          {
            title: 'index-layout.menu.home',
            path: '/home',
          },
          {
            title: 'index-layout.menu.home.custom-breadcrumbs.liqingsong.cc',
            // path: 'http://liqingsong.cc',
            path: '/home',
          },
        ],
        tabNavCloseBefore: (close: () => void): void=> {
          if(window.confirm('确认关闭吗')) {
            close();
          }
        }
      },
      {
        hidden: false,
        icon: 'detail',
        title: 'index-layout.menu.home.docs',
        path: 'http://admin-element-vue.liqingsong.cc/',
        belongTopMenu: '/home',
        redirect: ''
      },
    ],
  },
// 组件示例
  {
    hidden: true,
    icon: 'components',
    title: 'index-layout.menu.component',
    path: '/component',
    redirect: '/component/icon/svg',
    component: BlankLayout,
    children:[
        {
            hidden: true,
            icon: 'icon',
            title: 'index-layout.menu.component.icon',
            path: 'icon',
            redirect: '/component/icon/svg',
            component: BlankLayout,
            children: [
                {
                    hidden: true,
                    title: 'index-layout.menu.component.icon.svg',
                    path: 'svg',
                    component: () => import('@/views/component/icon/svg/index.vue'),
                },
                {
                  title: 'index-layout.menu.component.icon.font',
                  path: 'font',
                  component: () => import('@/views/component/icon/font/index.vue'),
                },
            ]
        },
        {
            hidden: true,
            icon: 'editor',
            title: 'index-layout.menu.component.editor',
            path: 'editor',
            redirect: '/component/editor/tuieditor',
            component: BlankLayout,
            children: [
                {
                    title: 'index-layout.menu.component.editor.tui-editor',
                    path: 'tuieditor',
                    component: () => import('@/views/component/editor/tui-editor/index.vue'),
                },
                {
                    title: 'index-layout.menu.component.editor.ckeditor',
                    path: 'ckeditor',
                    component: () => import('@/views/component/editor/ckeditor/index.vue'),
                }
            ]
        }
    ]
  },
// 页面示例
  {
    hidden: true,
    icon: 'page',
    title: 'index-layout.menu.pages',
    path: '/pagesample',
    redirect: '/pagesample/list/table',
    component: BlankLayout,
    children: [
    {
        hidden: true,
        icon: 'list',
        title: 'index-layout.menu.pages.list',
        path: 'list',
        redirect: '/pagesample/list/table',
        component: BlankLayout,
        children: [          
          {
            title: 'index-layout.menu.pages.list.table',
            path: 'table',
            component: ()=> import('@/views/pagesample/list/table/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.list.highly-adaptive-table',
            path: 'highlyadaptivetable',
            component: ()=> import('@/views/pagesample/list/highly-adaptive-table/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.list.search',
            path: 'search',
            redirect: '/pagesample/list/search/table',
            component: BlankLayout,
            children: [
              {
                title: 'index-layout.menu.pages.list.search.table',
                path: 'table',
                component: ()=> import('@/views/pagesample/list/search/table/index.vue'),
              }              
            ],
          },
        ],
      },
      {
        hidden: true,
        icon: 'edit',
        title: 'index-layout.menu.pages.form',
        path: 'form',
        redirect: '/pagesample/form/basic',
        component: BlankLayout,
        children: [
          {
            title: 'index-layout.menu.pages.form.basic',
            path: 'basic',
            component: ()=> import('@/views/pagesample/form/basic/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.form.complex',
            path: 'complex',
            component: ()=> import('@/views/pagesample/form/complex/index.vue'),
          },
        ],
      },
      {
        hidden: true,
        icon: 'detail',
        title: 'index-layout.menu.pages.detail',
        path: 'detail',
        redirect: '/pagesample/detail/basic',
        component: BlankLayout,
        children: [
          {
            title: 'index-layout.menu.pages.detail.basic',
            path: 'basic',
            component: ()=> import('@/views/pagesample/detail/basic/index.vue'),
            tabNavType: 'querypath',
          },
          {
            title: 'index-layout.menu.pages.detail.module',
            path: 'module',
            component: ()=> import('@/views/pagesample/detail/module/index.vue'),
          },
          {
            title: 'index-layout.menu.pages.detail.table',
            path: 'table',
            component: ()=> import('@/views/pagesample/detail/table/index.vue'),
          },
        ],
      },
    ],
  },
// 权限验证
  {
    hidden: true,
    icon: 'permissions',
    title: 'index-layout.menu.roles',
    path: '/roles',
    redirect: '/roles/all',
    component: BlankLayout,
    children: [
      {
        hidden: true,
        icon: 'detail',
        title: 'index-layout.menu.roles.all',
        path: 'all',
        component: ()=> import('@/views/roles/all/index.vue'),
      },
      {
        hidden: true,
        icon: 'detail',
        roles: ['user'],
        title: 'index-layout.menu.roles.user',
        path: 'user',
        component: ()=> import('@/views/roles/user/index.vue'),
      },
      {
        hidden: true,
        icon: 'detail',
        roles: ['test'],
        title: 'index-layout.menu.roles.test',
        path: 'test',
        component: ()=> import('@/views/roles/test/index.vue'),
      },
    ],
  },
  // ============================================
  // 馆员工作站
  {
    hidden: false,
    icon: 'page',
    title: '馆员工作站',
    path: '/rfid-workstation',
    component: BlankLayout,
    children: [
      {
        hidden: false,
        icon: 'list',
        title: '借阅者管理',
        path: 'rfid-lender-mgr',
        component: BlankLayout,
        children: [          
          {
            icon: 'page',
            hidden: false,
            title: '借阅者信息查询',
            path: 'rfid-lender-query',
            component: ()=> import('@/views/rfid-workstation/rfid-lender-mgr/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '借阅者信息注册',
            path: 'rfid-lender-register',
            component: ()=> import('@/views/rfid-workstation/rfid-lender-mgr/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '借阅者信息维护',
            path: 'rfid-lender-maintain',
            component: ()=> import('@/views/rfid-workstation/rfid-lender-mgr/index.vue'),       
          },
          {
            icon:'page',
            hidden: false,
            title: '借阅者注销',
            path: 'rfid-lender-deregist',
            component: ()=> import('@/views/rfid-workstation/rfid-lender-mgr/index.vue'),
          },
        ],
      },
      {
        hidden: false,
        icon: 'edit',
        title: '流通管理',
        path: 'rfid-circulate-mgr',
        component: BlankLayout,
        children: [
          {
            icon:'page',
            hidden: false,
            title: '档案借阅',
            path: 'rfid-circulate-borrow',
            component: ()=> import('@/views/rfid-workstation/rfid-circulate-mgr/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '案归还',
            path: 'rfid-circulate-return',
            component: ()=> import('@/views/rfid-workstation/rfid-circulate-mgr/index.vue'),
          },
        ],
      },
      {
        hidden: false,
        icon: 'detail',
        title: '信息查询',
        path: 'rfid-info-query',
        component: BlankLayout,
        children: [
          {
            icon:'page',
            hidden: false,
            title: '借阅者查询',
            path: 'rfid-lender-info-query',
            component: ()=> import('@/views/rfid-workstation/rfid-info-query/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '档案查询',
            path: 'rfid-archive-info-query',
            component: ()=> import('@/views/rfid-workstation/rfid-info-query/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '借阅查询',
            path: 'rfid-borrow-info-query',
            component: ()=> import('@/views/rfid-workstation/rfid-info-query/index.vue'),
          }, 
          {
            icon:'page',
            hidden: false,
            title: '借阅统计',
            path: 'rfid-borrow-statistics',
            component: ()=> import('@/views/rfid-workstation/rfid-info-query/index.vue'),
          }
        ],
      },
    ],
  },
  // RFID盘点车
  {
    hidden: false,
    icon: 'page',
    title: 'RFID盘点车',
    path: '/rfid-inventory-car',
    component: BlankLayout,
    children: [
      {
        hidden: false,
        icon: 'list',
        title: 'RFID上下架',
        path: 'rfid-car-up-down',
        component: BlankLayout,
        children: [          
          {
            icon:'page',
            hidden: false,
            title: '新档案上架',
            path: 'rfid-car-new-archive-up',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-up-down/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '档案上架',
            path: 'rfid-car-archive-up',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-up-down/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '档案下架',
            path: 'rfid-car-archive-down',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-up-down/index.vue'),
          }
        ],
      },
      {
        hidden: false,
        icon: 'edit',
        title: 'RFID盘点',
        path: 'rfid-car-inventory',
        component: BlankLayout,
        children: [          
          {
            icon:'page',
            hidden: false,
            title: '档案盘点',
            path: 'rfid-car-inventory',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-inventory/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '盘点结果',
            path: 'rfid-car-inventory-result',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-inventory/index.vue'),
          },
        ],
      },
      {
        hidden: false,
        icon: 'detail',
        title: 'RFID倒架',
        path: 'rfid-car-change-shelf',
        // redirect: '/pagesample/detail/basic',
        component: BlankLayout,
        children: [
          {
            icon:'page',
            hidden: false,
            title: '顺架管理',
            path: 'rfid-car-check-shelf',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-change-shelf/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '档案倒架',
            path: 'rfid-car-archive-change-shelf',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-change-shelf/index.vue'),
          },        
         
        ],
      },
      {
        hidden: false,
        icon: 'detail',
        title: '盘点查询',
        path: 'rfid-car-query',
        component: BlankLayout,
        children: [
          {
            icon:'page',
            hidden: false,
            title: 'RFID盘点查询',
            path: 'rfid-car-query-inventory',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-query/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '标签测试',
            path: 'rfid-car-label-test',
            component: ()=> import('@/views/rfid-inventory-car/rfid-car-query/index.vue'),
          },
         
        ],
      },
     
    ],
  },  
  // RFID标签管理
  {
    hidden: false,
    icon: 'page',
    title: 'RFID标签管理',
    path: '/rfid-label-mgr',
    component: BlankLayout,
    children: [
      {
        hidden: false,
        icon: 'list',
        title: '档案标签管理',
        path: 'rfid-archives-label-mgr',
        component: BlankLayout,
        children: [          
          {
            icon:'page',
            hidden: false,
            title: '档案标签注册',
            path: 'rfid-archives-label-register',
            component: ()=> import('@/views/rfid-labelmgr/rfid-archives-label/label-register/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '档案标签更换',
            path: 'rfid-archives-label-change',
            component: ()=> import('@/views/rfid-labelmgr/rfid-archives-label/label-change/index.vue'),
          },
          {
            icon:'page',
            hidden: false,
            title: '档案标签注销',
            path: 'rfid-archives-label-deregist',
            component: ()=> import('@/views/rfid-labelmgr/rfid-archives-label/label-register/index.vue'),  
          }
        ],
      },
      {
        hidden: false,
        icon: 'edit',
        title: '层架标签管理',
        path: 'rfid-layer-label-mgr',
        component: BlankLayout,
        children: [          
          {
            icon:'page',
            hidden: false,
            title: '层架标签注册',
            path: 'rfid-layer-label-register',
            component: ()=> import('@/views/rfid-labelmgr/rfid-layer-label/index.vue'),  
          },
          {
            icon:'page',
            hidden: false,
            title: '层架标签更换',
            path: 'rfid-layer-label-change',
            component: ()=> import('@/views/rfid-labelmgr/rfid-layer-label/index.vue'),  
          },
          {
            icon:'page',
            hidden: false,
            title: '层架标签注销',          
            path: 'rfid-layer-label-deregist',
            component: ()=> import('@/views/rfid-labelmgr/rfid-layer-label/index.vue'),  
          }
        ],
      },
      {
        hidden: false,
        icon: 'detail',
        title: '查询统计',
        path: 'detail',
        component: BlankLayout,
        children: [
          {
            icon:'page',
            hidden: false,
            title: '注册查询',
            path: 'rfid-label-register-query',
            component: ()=> import('@/views/rfid-labelmgr/rfid-label-query/index.vue'),  
   
            tabNavType: 'querypath',
          },
          {
            icon:'page',
            hidden: false,
            title: '注册统计',
            path: 'rfid-label-chart-query',
            component: ()=> import('@/views/rfid-labelmgr/rfid-label-query/index.vue'),  
          },
          {
            icon:'page',
            hidden: false,
            title: '交互信息查询',
            path: 'rfid-label-operator-query',
            component: ()=> import('@/views/rfid-labelmgr/rfid-label-query/index.vue'),  
          }
        ],
      },
      {
        hidden: false,
        icon: 'detail',
        title: '辅助工具',
        path: 'detail',
        // redirect: '/pagesample/detail/basic',
        component: BlankLayout,
        children: [
          {
            icon:'page',
            hidden: false,
            title: '标签分析',
            path: 'rfid-label-tool-analysis',
            component: ()=> import('@/views/rfid-labelmgr/rfid-label-tool/index.vue'),  
            tabNavType: 'querypath',
          },
          {
            icon:'page',
            hidden: false,
            title: '层位导入',
            path: 'rfid-label-tool-import',
            component: ()=> import('@/views/rfid-labelmgr/rfid-label-tool/index.vue'),  
          },
          {
            icon:'page',
            hidden: false,
            title: '交互信息导入',
            path: 'rfid-label-tool-infor-import',
            component: ()=> import('@/views/rfid-labelmgr/rfid-label-tool/index.vue'),  
          }, 
          {
            icon:'page',
            hidden: false,
            title: '系统配置',
            path: 'rfid-label-tool-setting',
            component: ()=> import('@/views/rfid-labelmgr/rfid-label-tool/index.vue'),  
          }
        ],
      },
    ],
  },
  // RFID系统维护
  {
    hidden: false,
    icon: 'page',
    title: 'RFID系统维护',
    path: '/rfid-sysmgr',
    // redirect: '/pagesample/list/table',
    component: BlankLayout,
    children: [
      {
        icon:'page',
        hidden: false,
        title: '系统维护',
        path: 'rfid-sys-maintain',
        component: ()=> import('@/views/rfid-sysmgr/sys-maintain/index.vue'),
      },
      {
        icon:'page',
        hidden: false,
        title: '系统设置',
        path: 'rfid-sys-setting',
        component: ()=> import('@/views/rfid-sysmgr/sys-setting/index.vue'),
      },
    ]
  }, 


];

export default IndexLayoutRoutes;